## Revolut's Feedback on Task

Feedback provided by Revolut team via email regarding the home task.

### Positive Feedback

- Correct answer to sql1 and written in maintainable state
- The nesting function handles correctly the following cases:
    - `cat input.json | python nest.py currency country city`
    - `cat input.json | python nest.py currency country aaa` --  throws useful error
    - `cat input.json | python nest.py currency country city city` -- throws useful error
- Readable code
- Pipfile for requirements
- README file 
- API works as expected and gives correct response codes
- API uses basic auth
- Good tests for nesting function including negative cases 


### Negative Feedback

- No answers to SQL tasks 2 & 3
- `cat input.json | python nest.py currency` doesn't nest correctly and throws an error "AttributeError: 'list' object has no attribute 'items'"
- Not PEP8 compliant (errors E501, E999, E255)
- Doesn't use type annotations
- API is not versioned
- Running `python app.py` gives the following error due to a typo.
    You have not been penalised for this as I addressed to check the rest of the functionality 
    but it's good to double check the submitted code in case your reviewers are stricter than me.
    ```
        def handle_response(data=None, /, message=None, status='success', code=200):
                                       ^
    SyntaxError: invalid syntax
    ```
