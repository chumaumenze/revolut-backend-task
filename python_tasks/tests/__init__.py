import os
import unittest
import json
import base64

from unittest.mock import Mock, patch

from app import app, verify_login
from nest import main, nest_input, nested_update
from util import handle_response, handle_exception, APIError


class BaseTestClass(unittest.TestCase):
    def setUp(self) -> None:
        current_path = os.path.abspath(os.path.dirname(__file__))
        with open(os.path.join(current_path, 'input.json')) as f:
            self.input_str = f.read()
        with open(os.path.join(current_path, 'output.json')) as f:
            self.output_str = f.read()

        self.input_json = json.loads(self.input_str)
        self.output_json = json.loads(self.output_str)
        self.nesting_levels = ['currency', 'country', 'city']


class TestNest(BaseTestClass):

    def setUp(self) -> None:
        super(TestNest, self).setUp()
        self.sys_mock = Mock(stdin=Mock(
            read=Mock(return_value=self.input_str)
        ))
        self.argparse_mock = Mock()
        self.parse_args_mock = Mock(
            return_value=Mock(nesting_levels=self.nesting_levels)
        )
        self.argparse_mock.ArgumentParser.return_value = Mock(
            parse_args=self.parse_args_mock, add_argument=Mock())
        self.json_mock = Mock(
            loads=Mock(return_value=self.input_json),
            dumps=Mock(return_value=self.output_json)
        )
        patches = patch.multiple(
            'nest', sys=self.sys_mock, json=self.json_mock,
            argparse=self.argparse_mock
        )
        patches.start()
        self.addCleanup(patch.stopall)

    def test_nest_input_with_valid_data(self):
        result = nest_input(self.input_json, self.nesting_levels)
        self.assertDictEqual(self.output_json, result)

    def test_nest_input_with_invalid_data(self):
        self.nesting_levels = ['not', 'a', 'valid', 'key']

        self.assertRaises(KeyError, nest_input,
                          self.input_json, self.nesting_levels)

    def test_nest_update_with_valid_data(self):
        expected_result = {'a': {'b': {'c': 5, 'd': [6], 'e': {'f': 3}}}}
        nested_dict = {'a': {'b': {'c': 1, 'd': [2], 'e': {'f': 3}}}}
        update_dict = {'a': {'b': {'c': 5, 'd': [6]}}}

        result = nested_update(nested_dict, update_dict)
        self.assertDictEqual(expected_result, result)

    def test_main(self):
        help_message = 'Creates a nested dict using specified keys'
        main()
        self.assertTrue(self.argparse_mock.ArgumentParser.assert_called_once)
        self.argparse_mock.ArgumentParser.return_value\
            .add_argument.assert_called_once_with(
                'nesting_levels', nargs='+', help=help_message)
        self.json_mock.loads.assert_called_once_with(self.input_str)
        self.sys_mock.stdout.write.assert_called_once_with(
            f'{self.output_json}\n')


class TestUtil(unittest.TestCase):

    def test_handle_response(self):
        expected_result = {
            'status': 'SUCCESS',
            'data': {'a': 1},
            'message': 'This is a test message'
        }, 200

        args = {'a': 1}, 'This is a test message'
        result = handle_response(*args)
        self.assertEqual(expected_result, result)

    def test_handle_exception_failure(self):
        expected_result = {
            'status': 'FAILURE',
            'data': None,
            'message': 'This is a test failure message'
        }, 400

        exception = APIError('This is a test failure message', code=400)
        result = handle_exception(exception)
        self.assertEqual(expected_result, result)

    def test_handle_exception_error(self):
        expected_result = {
            'status': 'ERROR',
            'data': None,
            'message': 'This is a test error message'
        }, 500

        exception = APIError('This is a test error message')
        result = handle_exception(exception)
        self.assertEqual(expected_result, result)

    def test_handle_builtin_exception(self):
        expected_result = {
            'status': 'ERROR',
            'data': None,
            'message': 'Internal server error'
        }, 500

        exception = ValueError()
        result = handle_exception(exception)
        self.assertEqual(expected_result, result)


class TestAPIError(unittest.TestCase):

    def test_api_error(self):
        message = 'Test message'
        code = 800
        self.api_error = APIError(message, code)
        self.assertEqual(self.api_error.message, message)
        self.assertEqual(self.api_error.code, code)

    def test_api_error_without_args(self):
        message = 'Internal server error'
        code = 500
        self.api_error = APIError()
        self.assertEqual(self.api_error.message, message)
        self.assertEqual(self.api_error.code, code)


class TestApp(BaseTestClass):

    def setUp(self) -> None:
        super(TestApp, self).setUp()

        encoded_credential = base64.b64encode(b'user:pass').decode('ascii')
        self.headers = {'Authorization': f'Basic {encoded_credential}'}

        self.app = app.test_client()

    def test_verify_login_with_valid_credentials(self):
        result = verify_login('user', 'pass')
        self.assertTrue(result)

    def test_verify_login_with_invalid_credentials(self):
        self.assertRaises(APIError, verify_login, 'user', 'wrongpass')

    def test_hello_world(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(200, response.status_code)
        self.assertIn(b'Hello World!', response.data)

    def test_create_nest(self):
        url_args = ','.join(self.nesting_levels)
        response = self.app.post(
            f'/nest?nesting-levels={url_args}', follow_redirects=True,
            json=self.input_json, headers=self.headers
        )
        self.assertEqual(200, response.status_code)
        self.assertIn(b'SUCCESS', response.data)

    def test_create_nest_keyerror(self):
        url_args = ','.join(['invalid_key'])
        response = self.app.post(
            f'/nest?nesting-levels={url_args}', follow_redirects=True,
            json=self.input_json, headers=self.headers
        )
        self.assertEqual(400, response.status_code)
        self.assertIn(b'FAILURE', response.data)
        self.assertIn(b"Key 'invalid_key' not found", response.data)

    def test_create_nest_missing_params(self):
        response = self.app.post(
            '/nest', follow_redirects=True,
            json=self.input_json, headers=self.headers
        )
        self.assertEqual(400, response.status_code)
        self.assertIn(b'FAILURE', response.data)
        self.assertIn(b"Missing required data.", response.data)

    def test_create_nest_unauthorized(self):
        url_args = ','.join(self.nesting_levels)
        response = self.app.post(
            f'/nest?nesting-levels={url_args}', follow_redirects=True,
            json=self.input_json
        )
        self.assertEqual(401, response.status_code)
        self.assertIn(b'FAILURE', response.data)
        self.assertIn(b"Unauthorized", response.data)

    def test_create_nest_recursionerror(self):
        url_args = ','.join(self.nesting_levels)
        with patch('nest.nest_input', Mock(side_effect=RecursionError)):
            response = self.app.post(
                f'/nest?nesting-levels={url_args}', follow_redirects=True,
                json=self.input_json, headers=self.headers
            )

        self.assertEqual(500, response.status_code)
        self.assertIn(b'ERROR', response.data)
        self.assertIn(b'Nesting limit reached.', response.data)
