#!/usr/bin/env python3

import argparse
import json
import sys

from collections import abc


def nest_input(input_data, nesting_levels=None):
    """
    Create a nested dict using specified keys as nesting levels

    @param list input_data: List of flat dictionary
    @param list nesting_levels: A list of immutable data as keys for `input_data`
    """
    nesting_levels = nesting_levels or []
    output_data = {}

    if not nesting_levels:
        return [input_data]

    if isinstance(input_data, list):
        for data in input_data:
            # Extract root-level data
            data_key = data.pop(nesting_levels[0])
            output_data.setdefault(data_key, {})

            # Recursively nest and update the root-level data
            root_level_data = nest_input(data, nesting_levels[1:])
            output_data[data_key] = nested_update(
                output_data[data_key], root_level_data)
    else:
        root_level_data = {}
        data_key = input_data.pop(nesting_levels[0])
        root_level_data[data_key] = nest_input(input_data, nesting_levels[1:])
        return root_level_data

    return output_data


def nested_update(data, values):
    """
    Recursively update deeply nested dictionary without data overwrite

    Stolen from https://stackoverflow.com/a/3233356
    """
    for k, v in values.items():
        if isinstance(v, abc.Mapping):
            data.setdefault(k, {})
            data[k] = nested_update(data[k], v)
        else:
            data[k] = v
    return data


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('nesting_levels', nargs='+',
                        help='Creates a nested dict using specified keys')

    args = parser.parse_args()
    input_json = json.loads(sys.stdin.read())
    levels = args.nesting_levels

    output_json = nest_input(input_json, levels)
    output_json = json.dumps(output_json, sort_keys=True, indent=2)

    sys.stdout.write(f'{output_json}\n')


if __name__ == '__main__':
    main()
