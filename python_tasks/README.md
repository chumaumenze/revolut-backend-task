## Nest.py

A program that will parse a json object, and return a nested dictionary of dictionaries of arrays, 
with keys specified in command line arguments and the leaf values as arrays of flat dictionaries 
matching appropriate groups

#### From Command Line

```bash
cat input.json | python nest.py currency country city
```

#### Run the Nest Server

```bash
python app.py
```
##### Endpoints

+ `/`: Test API Connectivity
+ `/nest`
    + Process inputs
    + Requires HTTP Basic Authentication
    + Requires JSON payload with array of flat JSON objects
    + Requires URL Args `nesting-levels` of comma-separated nesting keys. (i.e. `currency,country,city`)


#### Default credential

+ Username: `user`
+ Password: `pass`


#### Testing

```bash
nose2 --with-coverage ./tests
```