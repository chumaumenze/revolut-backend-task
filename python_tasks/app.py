import json

from flask import Flask, request
from flask_httpauth import HTTPBasicAuth
from werkzeug.exceptions import HTTPException
from werkzeug.security import generate_password_hash, check_password_hash

from nest import nest_input
from util import APIError, handle_response, handle_exception


app = Flask(__name__)
app.register_error_handler(APIError, handle_exception)
app.register_error_handler(HTTPException, handle_exception)

api_auth = HTTPBasicAuth()


@api_auth.verify_password
def verify_login(username, password):
    """
    Mimics a simple basic authentication verification

    !!! PLEASE DO NOT USE ON PRODUCTION!!!
    Default username: user
    Default password: pass
    Implemented to skip database creation, password hash etc
    !!! PLEASE DO NOT USE ON PRODUCTION!!!
    """
    users = {
        'user': generate_password_hash('pass')
    }
    if check_password_hash(users.get(username, ''), password):
        return True

    raise APIError('Unauthorized Access', 401)


@app.route('/')
def hello_world():
    return handle_response(message='Hello World!')


@app.route('/nest', methods=['POST'])
@api_auth.login_required
def create_nest():
    input_data = request.get_json()
    nesting_levels = request.args.get('nesting-levels', None)

    if not (input_data and nesting_levels):
        raise APIError('Missing required data.', 400)

    nesting_levels = nesting_levels.split(',')
    try:
        output_data = nest_input(input_data, nesting_levels)
        output_data = json.dumps(output_data, sort_keys=True)
    except KeyError as e:
        error_message = f"Key {e} not found."
        raise APIError(error_message, 400)
    except RecursionError:  # Failsafe for any recursion error
        error_message = f"Nesting limit reached."
        raise APIError(error_message, 500)

    return handle_response(output_data)


if __name__ == '__main__':
    app.run()
