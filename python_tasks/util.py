def handle_response(data=None, /, message=None, status='success', code=200):
    return {
        'status': status.upper(),
        'data': data,
        'message': message
    }, code


def handle_exception(exception):
    code = getattr(exception, 'code', 500)
    message = getattr(
        exception, 'description',
        getattr(exception, 'message', APIError.message)
    )

    exception_type = 'ERROR' if str(code)[:1] == '5' else 'FAILURE'

    return handle_response(message=message, status=exception_type,
                           code=code)


class APIError(Exception):
    """
    Base API error class
    """
    code = 500
    message = 'Internal server error'

    def __init__(self, message=None, code=None):
        self.message = message or getattr(self.__class__, 'message', None)
        self.code = code or getattr(self.__class__, 'code', None)
