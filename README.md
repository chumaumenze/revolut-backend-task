## Revolut Home Task Solution

Code solution to Revolut Python Engineer

See:
+ [Task Description](./DESCRIPTION.md) for the home task description.
+ [Python programming solution](./python_tasks).
+ [SQL solution](./sql_tasks).
+ [Revolut feedback](./FEEDBACK.md) for current solution.
